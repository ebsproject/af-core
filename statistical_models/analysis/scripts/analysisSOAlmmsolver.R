# -------------------------------------------------------------------------------------
# Name             : analysisSOAlmmsolver
# Description      : Execute the Single Occurrence Analysis which links the UI input to
#                  : EBSAnalytics package
# R Version        : 4.2.3 
# Note             : Data coming from CB and uses the column names 
# -------------------------------------------------------------------------------------
# Author           : Alaine A. Gulles 
# Author Email     : a.gulles@irri.org
# Date             : 2023.05.18
# Date Modified    : 2023.05.18
# Maintainer       : Alaine A. Gulles 
# Maintainer Email : a.gulles@irri.org
# Script Version   : 1
# Command          : Rscript analysisSOAlmmsolver.R --design "Alpha-Lattice"
#                    --inputFile "C:/Sample Data/Loc 13463_Yld_V2.csv" --trait "YLD_CONT_TON" 
#                    --outputPath "D:/Results/"
# -------------------------------------------------------------------------------------
# Parameters:
# design = name of the experimental design
# inputFile = a cvs file containing the entry information
# trait = response variable to be analyze
# outputPath = path where output will be saved
# ---------------------------------------------------------

# load the needed packages
suppressWarnings(suppressPackageStartupMessages(library(optparse)))
suppressWarnings(suppressPackageStartupMessages(library(EBSAnalytics)))

optionList <- list(
  make_option(opt_str = c("--design"), type = "character", 
              help = "Design", metavar = "experimental design"),
  make_option(opt_str = c("-i", "--inputFile"), type = "character", 
              help = "Location and filename of the data to be use for analysis",
              metavar = "Data"),
  make_option(opt_str = c("-t","--trait"), type = "character",
              help = "Number of trials", metavar = "number of trials"),
  make_option(opt_str = c("-p", "--outputPath"), type = "character", default = getwd(),
              help = "Path where output will be saved",
              metavar = "path where output will be saved")
)

# garbage collection 
tmpGC <- gc()

# create an instance of a parser object
opt_parser = OptionParser(option_list = optionList)
opt = parse_args(opt_parser)

# check if folder is exist or not
if (!dir.exists(opt$outputPath)) {
  dir.create(opt$outputPath)
}

sink(file = paste0(opt$outputPath, "analysisInfo.txt"))

temp <- try(result <- soaLMM(design = opt$design,
                             filename = opt$inputFile,
                             trait = opt$trait), 
            silent = TRUE)

if(all(class(temp) == "try-error")) {
  msg <- trimws(strsplit(temp, ":")[[1]])
  msg <- trimws(paste(strsplit(msg, "\n")[[length(msg)]], collapse = " "))
  cat("Error in soaLMM:", msg, sep = "")
}

sink()


saveRDS(result$entryFixed, file = paste0(opt$outputPath,"entryAsFixed.rds"))
saveRDS(result$entryRndom, file = paste0(opt$outputPath,"entryAsRandom.rds"))

write.csv(result$dataWResid, file = paste0(opt$outputPath,"Residuals.csv"), row.names = F)
write.csv(result$analysisParam, file = paste0(opt$outputPath,"Analysis Parameter.csv"), row.names = F)
write.csv(result$entryFixed$predict, file = paste0(opt$outputPath,"SOA Predictions_Fixed.csv"), row.names = F)
write.csv(result$entryRndom$predict, file = paste0(opt$outputPath,"SOA Predictions_Random.csv"), row.names = F)

