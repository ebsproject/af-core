# -------------------------------------------------------------------------------------
# Name             : soaLMM
# Description      : Wrapper function that performs SOA using LMMsolver
# R Version        : 4.2.3
# Pkg Dependency   : LMMsolver 1.0.5
# Note             : 
# -------------------------------------------------------------------------------------
# Author           : Alaine A. Gulles 
# Author Email     : a.gulles@irri.org
# Date             : 2023.05.10
# Date Modified    : 2023.05.16
# Maintainer       : Alaine A. Gulles 
# Maintainer Email : a.gulles@irri.org
# Script Version   : 1.1
# Command          : 
# -------------------------------------------------------------------------------------
#' @name soaLMM
#' @aliases soaLMM
#' @title soaLMM
#' 
#' @description Function the performs Single Occurrence Analysis using LMMsolver
#' @param design a character value; value comes from CB DB
#' @param filename a string indicating the path and name of the file to be used in the analysis;          if it includes the path, uses "/"; currently accepts csv file
#' @param trait a character value indicating the trait that will be use to build the model
# -------------------------------------------------------------------------------------

soaLMM <- function(design = c("RCBD", "Alpha-Lattice", "Row-Column", "Augmented RCBD","Augmented Design", 
                              "Partially Replicated"),
                   filename,
                   trait) {
  
  design <- match.arg(design)
  
  # # load the needed package
  # library(LMMsolver) # Built under R Version 4.2.3
  
  # load the data
  mydata <- read.csv(filename)
  
  # checks the trait if its in the file
  if(!trait %in% colnames(mydata)) { stop(paste("The trait = ",trait," is not one of the columns in the dataset.")) }
  
  # check if the columns are present depending on the design
  
  # determine the model
  myfxn <- buildModel(design, mydata, trait)
  
  # converts the columns except the last to factor
  for (i in (1:ncol(mydata))) {
    if (myfxn$hasCoor) {
      if (names(mydata)[i] == "pa_y" | names(mydata)[i] == "pa_x" | names(mydata)[i] == trait) {
        next
      }
    } else {
      if (names(mydata)[i] == trait) {
        next
      }
    }
    mydata[,i] <- as.factor(mydata[,i])
  }
  
  # Fit the model
  # geno is fixed
  tempF <- try(myModel.fix <- runSOAlmm(data = mydata, 
                                        fixedStmt = myfxn$modelComponent$entryFixed.fixedPart,
                                        rndomStmt = myfxn$modelComponent$entryFixed.rndomPart,
                                        hasCoor = myfxn$hasCoor,
                                        isEntryRandom = FALSE, 
                                        maxIter = 100),
               silent = TRUE)
  
  # entry is random
  tempR <- try(myModel.rnd <- runSOAlmm(data = mydata, 
                                        fixedStmt = myfxn$modelComponent$entryRandom.fixedPart,
                                        rndomStmt = myfxn$modelComponent$entryRandom.rndomPart,
                                        hasCoor = myfxn$hasCoor,
                                        isEntryRandom = TRUE, 
                                        maxIter = 100), 
               silent = TRUE)
  
  if(all(class(tempF) == "try-error")) {
    msg <- trimws(strsplit(temp, ":")[[1]])
    msg <- trimws(paste(strsplit(msg, "\n")[[length(msg)]], collapse = " "))
    # cat("Error in soaLMM:", msg, sep = "")
    stop(paste("Error in soaLMM:", msg, sep = ""))
  }
  
  if(all(class(tempR) == "try-error")) {
    msg <- trimws(strsplit(temp, ":")[[1]])
    msg <- trimws(paste(strsplit(msg, "\n")[[length(msg)]], collapse = " "))
    # cat("Error in soaLMM:", msg, sep = "")
    stop(paste("Error in soaLMM:", msg, sep = ""))
  }
  
  dataResid <- data.frame(mydata, RESID.BLUEs = myModel.fix$residual,
                          RESID.BLUPs = myModel.rnd$residual)
  analysisParamAll <- merge(myModel.fix$analysisParam,
                            myModel.rnd$analysisParam,
                            all = TRUE)
  
  return(list(entryFixed = myModel.fix,
              entryRndom = myModel.rnd,
              dataWResid = dataResid,
              analysisParam = analysisParamAll))
}
# end of soaLMM function