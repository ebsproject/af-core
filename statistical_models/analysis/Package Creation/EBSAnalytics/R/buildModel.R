# -------------------------------------------------------------------------------------
# Name             : buildModel
# Description      : Construct the model that will be used for LMMsoler 
#                  : Script assume that the dataset will be coming from CB and uses
#                    the column names when we download the dataset from CB DB
# R Version        : 4.2.3
# Pkg Dependency   : LMMsolver 1.0.5
# Note             : 
# -------------------------------------------------------------------------------------
# Author           : Alaine A. Gulles 
# Author Email     : a.gulles@irri.org
# Date             : 2023.05.10
# Date Modified    : 2023.05.16
# Maintainer       : Alaine A. Gulles 
# Maintainer Email : a.gulles@irri.org
# Script Version   : 1.1
# Command          : 
# -------------------------------------------------------------------------------------
#' @name buildModel
#' @aliases buildModel
#' @title buildModel
#' 
#' @description Build model for LMMsolver
#' @param design a character value; value comes from CB DB
#' @param data an R object of class data.frame; data from one occurrence from CB DB
#' @param trait a character value indicating the trait that will be use to build the model
# -------------------------------------------------------------------------------------

buildModel <- function(design = c("RCBD", "Alpha-Lattice", "Row-Column", "Augmented RCBD","Augmented Design", 
                                  "Partially Replicated"), 
                       data, 
                       trait) {
  
  design <- match.arg(design)
  
  if (design == "RCBD") {
    entryRandom.fixedPart <- paste0(trait, " ~ rep_factor")           # rcbd, entry_id is random
    entryRandom.rndomPart <- "~ entry_id"                             # rcbd, entry_id is random
    entryFixed.fixedPart <- paste0(trait, " ~ rep_factor + entry_id") # rcbd, entry_id is fixed
    entryFixed.rndomPart <- NULL                                      # rcbd, entry_id is fixed
    
  } else if(design == "Alpha-Lattice") {
    entryRandom.fixedPart <- paste0(trait, " ~ rep_factor")     # alpha lattice, entry_id is random
    entryRandom.rndomPart <- "~ entry_id + rep_factor:blk"          # alpha lattice, entry_id is random
    entryFixed.fixedPart <- paste0(trait, " ~ rep_factor + entry_id") # alpha lattice, entry_id is fixed
    entryFixed.rndomPart <- "~ rep_factor:blk"                # alpha lattice, entry_id is fixed
  
  } else if (design == "Row-Column") {
    entryRandom.fixedPart <- paste0(trait, " ~ rep_factor")              # pa_y-column, entry_id is random
    entryRandom.rndomPart <- "~ entry_id + rep_factor:rowblk + rep_factor:colblk" # pa_y-column, entry_id is random
    entryFixed.fixedPart <- paste0(trait, " ~ rep_factor + entry_id")          # pa_y-column, entry_id is fixed
    entryFixed.rndomPart <- "~ rep_factor:rowblk + rep_factor:colblk"       # pa_y-column, entry_id is fixed
    
  } else if (design == "Augmented RCBD") {
    # assume that blk contains the correct info
    entryRandom.fixedPart <- paste0(trait, " ~ blk")     # augrcbd, entry_id is random
    entryRandom.rndomPart <- "~ entry_id"                        # augrcbd, entry_id is random
    entryFixed.fixedPart <- paste0(trait, " ~ blk + entry_id") # augrcbd, entry_id is fixed
    entryFixed.rndomPart <- NULL                           # augrcbd, entry_id is fixed
    
  } else if (design == "Augmented Design" || design == "Partially Replicated") {
    entryRandom.fixedPart <- paste0(trait, " ~ 1")         # augmented or p-rep_factor, entry_id is random
    entryRandom.rndomPart <- "~ entry_id"                        # augmented or p-rep_factor, entry_id is random
    entryFixed.fixedPart <- paste0(trait, " ~ entry_id")         # augmented or p-rep_factor, entry_id is fixed
    entryFixed.rndomPart <- NULL                           # augmented or p-rep_factor, entry_id is fixed
  }
  
  modelComponent <- list()
  modelComponent$entryFixed.fixedPart <- entryFixed.fixedPart
  modelComponent$entryFixed.rndomPart <- entryFixed.rndomPart
  modelComponent$entryRandom.fixedPart <- entryRandom.fixedPart
  modelComponent$entryRandom.rndomPart <- entryRandom.rndomPart
  
  # ----- checks if there are coordinates in the dataset
  hasCoor <- FALSE
  if(!is.na(match("pa_y", names(data))) & !is.na(match("pa_x", names(data)))) {
    checkCoor <- with(data, table(pa_y, pa_x)) 
    if (nrow(checkCoor) > 1 & ncol(checkCoor) > 1){
      noCoor <- which(checkCoor != 1, arr.ind = TRUE)   # checks if there are missing coordinates or duplicates
      if (nrow(noCoor) == 0) hasCoor <- TRUE
    }
  }
  
  return(list(modelComponent = modelComponent, hasCoor = hasCoor))
} 
# end of buildModel function