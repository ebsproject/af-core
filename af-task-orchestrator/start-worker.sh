#!/bin/sh

# Activate asreml license
echo $ASREML_ACTIVATION_CODE
# asreml -z $ASREML_ACTIVATION_CODE
Rscript /app/packages/activate_license.R

supervisord -c supervisord.cfg
