import logging
import os

log = logging.getLogger(__name__)
AFDB_URI = os.getenv("AFDB_URL")

UNIVERSAL_UNKNOWN = "NA"

ANALYZE_IMPLEMENTATIONS = {
    "asreml": "af.pipeline.asreml.analyze.AsremlAnalyze",
    "asreml-r": "af.pipeline.asreml_r.analyze.AsremlRAnalyze",
    "sommer": "af.pipeline.sommer.analyze.SommeRAnalyze",
}

# expt loc patterns
SESL = "SESL"
SEML = "SEML"
MESL = "MESL"
MEML = "MEML"


def get_afdb_uri():
    return os.getenv("AFDB_URL")


def get_analysis_engine_script(engine_name: str):
    # This needs to configured from db
    engine = engine_name.lower()

    # Or this can just be defined by their respective Analyze classes
    if engine == "asreml":
        return "asreml"

    if engine in ["asremlr", "asreml-r", "asreml_r"]:
        return "asreml-r"

    if engine in ["r - sommer", "sommer"]:
        return "sommer"

    return None


def get_analyze_class(engine_name):
    """Gets the configured analyze class"""
    #
    log.info(f"Getting Analyze class impl for engine: {engine_name}")
    kls = ANALYZE_IMPLEMENTATIONS.get(engine_name.lower())
    parts = kls.split(".")
    module = ".".join(parts[:-1])
    m = __import__(module)
    for comp in parts[1:]:
        m = getattr(m, comp)
    return m
