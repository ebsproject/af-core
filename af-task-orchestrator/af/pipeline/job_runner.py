import abc

from af.pipeline.job_data import JobData


class JobRunner(abc.ABC):
    _DEFAULT_CONVERGENCE_TRIES = 6

    def __init__(self, analyze_obj, job_data: JobData, analysis_engine=None):
        self.analyze_obj = analyze_obj
        self.job_data = job_data
        self.analysis_engine = analysis_engine

    def get_convergence_tries(self) -> int:
        return self._DEFAULT_CONVERGENCE_TRIES

    @abc.abstractmethod
    def run_job(self, **kwargs):
        pass
