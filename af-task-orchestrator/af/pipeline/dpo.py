#!/usr/bin/env python3

import argparse
import json
import logging
import os

# import pathlib
import sys
from abc import ABC, abstractmethod

# from collections import OrderedDict
from os import path

from pydantic import ValidationError

from af.pipeline.data_reader.exceptions import DataReaderException

if os.getenv("PIPELINE_EXECUTOR") is not None and os.getenv("PIPELINE_EXECUTOR") == "SLURM":
    file_dir = path.dirname(os.path.realpath(__file__))
    pipeline_dir = path.dirname(file_dir)
    sys.path.append(pipeline_dir)

import pathlib

from af.pipeline.analysis_request import AnalysisRequest
from af.pipeline.data_reader import DataReaderFactory, PhenotypeData
from af.pipeline.data_reader.models import Trait  # noqa: E402; noqa: E402
from af.pipeline.db.core import DBConfig
from af.pipeline.exceptions import DpoException, InvalidAnalysisRequest

from .config import MEML, MESL, SEML, SESL

log = logging.getLogger(__name__)


class ProcessData(ABC):
    """Abstract class for ProcessData objects"""

    def __init__(self, analysis_request, exptloc_analysis_pattern=SESL, **kwargs):
        """Constructor.

        Args:
            analysis_request: Object with all required inputs to run analysis.
        """

        self.analysis_request = analysis_request
        self.exptloc_analysis_pattern = exptloc_analysis_pattern

        factory = DataReaderFactory(analysis_request.dataSource.name)
        self.data_reader: PhenotypeData = factory.get_phenotype_data(
            api_base_url=analysis_request.dataSourceUrl, api_bearer_token=analysis_request.dataSourceAccessToken
        )

        self.experiment_ids = []
        self.occurrence_ids = []
        self.location_ids = []

        # extract ids from the experiment, occurrence and location ids from analysis request.
        _location_ids = set()
        for experiment in analysis_request.experiments:
            self.experiment_ids.append(experiment.experimentId)
            if experiment.occurrences is not None:
                for occurrence in experiment.occurrences:
                    self.occurrence_ids.append(occurrence.occurrenceId)
                    _location_ids.add(occurrence.locationId)
        self.location_ids = sorted(_location_ids)

        self.trait_ids = []
        self.trait_by_id = {}
        for trait in analysis_request.traits:
            self.trait_ids.append(trait.traitId)

        self.db_session = DBConfig.get_session()

        self.analysis_fields = None
        self.input_fields_to_config_fields = None

        self.output_folder = analysis_request.outputFolder

    def get_job_folder(self, job_name: str) -> str:

        job_folder = os.path.join(self.output_folder, job_name)

        if not os.path.isdir(job_folder):
            # create parent directories
            os.makedirs(pathlib.Path(job_folder))

        return job_folder

    def get_meta_data_file_path(self, job_name: str) -> str:
        job_folder = self.get_job_folder(job_name)
        return os.path.join(job_folder, "metadata.tsv")

    def get_trait_by_id(self, trait_id: str) -> Trait:

        if trait_id not in self.trait_by_id:
            log.info(f"Getting trait data from CB for variableId: {trait_id}")
            self.trait_by_id[trait_id] = self.data_reader.get_trait(trait_id)

        return self.trait_by_id[trait_id]

    @abstractmethod
    def sesl(self):
        pass

    @abstractmethod
    def seml(self):
        pass

    @abstractmethod
    def mesl(self):
        pass

    @abstractmethod
    def meml(self):
        pass

    def run(self, *args, **kwargs):
        """Pre process input data before inputing into analytical engine.

        Extracts plots and plot measurements from api source.
        Prepares the extracted data to feed into analytical engine.

        Returns:
            List of JobData
            example:
                [
                    JobData(
                        job_name: str = "",
                        job_result_dir: str = "",
                        data_file: str = "",
                        job_file: str = "",
                        job_params: JobParams = JobParams(
                            formula: str = None,
                            residual: str = None,
                            predictions: list[str] = None,
                        )
                        metadata_file: str = "",
                        occurrences: list[Occurrence] = field(default_factory=list),
                        trait_name: str = "",
                        location_name: str = ""
                    )
                ]

        Raises:
            DpoException, DataReaderException
        """

        try:
            return self._run(*args, **kwargs)
        except KeyError as ke:
            # this means the column may be incorrect
            log.error(f"Preprocessing error: {str(ke)}")
            raise DpoException("Model is not yet supported or analysis config fields maybe incorrect.")
        except (DpoException, DataReaderException):
            raise

    def _run(self, *args, **kwargs):
        job_inputs = []

        if self.exptloc_analysis_pattern == SESL:
            job_inputs_gen = self.sesl()
        elif self.exptloc_analysis_pattern == SEML:
            job_inputs_gen = self.seml()
        elif self.exptloc_analysis_pattern == MESL:
            job_inputs_gen = self.mesl()
        elif self.exptloc_analysis_pattern == MEML:
            job_inputs_gen = self.meml()
        else:
            raise DpoException(f"Analysis pattern value: {self.exptloc_analysis_pattern} is invalid")

        for job_input in job_inputs_gen:
            job_inputs.append(job_input)

        return job_inputs


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Process input data to feed into analytical engine")

    parser.add_argument("--request_file", help="File path for analysis request")

    args = parser.parse_args()

    if path.exists(args.request_file):
        with open(args.request_file) as f:
            try:
                analysis_request: AnalysisRequest = AnalysisRequest(**json.load(f))
            except ValidationError as e:
                raise InvalidAnalysisRequest(str(e))
    else:
        raise InvalidAnalysisRequest(f"Request file {args.request_file} not found")

    ProcessData(analysis_request).run()
