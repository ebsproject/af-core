import logging
from os import path

import rpy2.robjects as robjects
from af.pipeline import analysis_report, calculation_engine, report_constants, utils
from af.pipeline.analyze import ResultProcessor
from af.pipeline.asreml.analyze import AsremlAnalyze
from af.pipeline.asreml_r.asreml_r_result import AsremlRResult
from af.pipeline.asreml_r.dpo import AsremlRMemlProcessData, AsremlRProcessData
from af.pipeline.asreml_r.job_runners import (
    AsremlMEMLRandomEffectJobRunner,
    AsremlRJobRunner,
    AsremlRMEFixedEffectJobRunner,
    AsremlRMERandomEffectJobRunner,
)
from af.pipeline.asreml_r.models import AsremlRJobResult
from af.pipeline.config import MEML, MESL, SEML, SESL  # temporary while we refactor parts
from af.pipeline.db import services as db_services
from af.pipeline.job_data import JobData
from af.pipeline.job_runner import JobRunner
from af.pipeline.job_status import JobStatus

log = logging.getLogger(__name__)


class DefaultAsremlResultProcessor(ResultProcessor):
    def __init__(self, analyze_obj):
        self.analyze_obj = analyze_obj

    def create_predictions_reports(self, asreml_result, job_result: AsremlRJobResult, metadata_df):
        # write entry predictions to the report
        self.create_entry_predictions_report(asreml_result.entry_predictions, metadata_df)

        # write location predictions to the report
        self.create_location_predictions_report(asreml_result.location_predictions, metadata_df)

        # write entry location predictions
        self.create_entry_location_predictions_report(asreml_result.entry_location_predictions, metadata_df)

        # # write this for Entry report
        self.create_location_germplasm_report(
            asreml_result.location_germplasm_predictions,
            job_result,
            metadata_df,
            sheet_name=report_constants.ENTRY_SHEET_NAME,
            estimate_type="",
        )

    def create_location_germplasm_report(
        self, location_germplasm_predictions, job_result, metadata_df, sheet_name, estimate_type
    ):
        if location_germplasm_predictions is not None:
            analysis_report.write_location_germplasm_predictions(
                self.analyze_obj.db_session,
                self.analyze_obj.analysis_request,
                self.analyze_obj.report_file_path,
                location_germplasm_predictions,
                metadata_df,
                sheet_name=sheet_name,
                estimate_type=estimate_type,
            )

    def create_entry_predictions_report(self, entry_predictions, metadata_df):
        # write entry predictions to the report
        if entry_predictions is not None:
            analysis_report.write_entry_predictions(
                self.analyze_obj.db_session,
                self.analyze_obj.analysis_request,
                self.analyze_obj.report_file_path,
                entry_predictions,
                metadata_df,
            )

    def create_location_predictions_report(self, location_predictions, metadata_df):
        if location_predictions is not None:
            analysis_report.write_location_predictions(
                self.analyze_obj.report_file_path, location_predictions, metadata_df
            )

    def create_entry_location_predictions_report(self, entry_location_predictions, metadata_df):
        if entry_location_predictions is not None:
            analysis_report.write_entry_location_predictions(
                self.analyze_obj.report_file_path, entry_location_predictions, metadata_df
            )

    def create_model_stat_report(self, job_id, asreml_result, job_result, metadata_df):
        # calculate h2 cullis for entry if analysis pattern is SESL
        # TODO: Refactor h2 calc here
        h2_cullis = None
        if self.analyze_obj.exptloc_analysis_pattern == SESL:

            variance = asreml_result.entry_variance
            avg_std_error = asreml_result.entry_average_standard_error

            if variance is not None and avg_std_error is not None:
                try:
                    h2_cullis = calculation_engine.get_h2_cullis(variance, avg_std_error)
                except ValueError as ve:
                    h2_cullis = str(ve)

        # write model statisics to analysis report
        rename_keys = {"log_lik": "LogL"}
        model_stat = asreml_result.model_stat

        analysis_report.write_model_stat(
            self.analyze_obj.report_file_path,
            model_stat,
            metadata_df,
            rename_keys,
            h2_cullis=h2_cullis,
            report_fields=self.get_model_stat_fields(),
        )

    def process_job_result(self, job_result: AsremlRJobResult, gathered_objects: dict = None):
        job = db_services.get_job_by_name(self.analyze_obj.db_session, job_result.job_name)

        asreml_result = AsremlRResult(job, job_result.asr_rds_file, job_result.prediction_rds_files)

        if not asreml_result.converged:
            db_services.update_job(
                self.analyze_obj.db_session,
                job,
                JobStatus.FAILED,
                "Failed to converge.",
            )
            return gathered_objects

        # initialize the report workbook
        if not path.isfile(self.analyze_obj.report_file_path):
            utils.create_workbook(self.analyze_obj.report_file_path, sheet_names=report_constants.REPORT_SHEETS)

        metadata_df = utils.get_metadata(job_result.metadata_file)

        self.create_predictions_reports(asreml_result, job_result, metadata_df)

        # write model_stat
        self.create_model_stat_report(job.id, asreml_result, job_result, metadata_df)

        utils.zip_dir(job_result.job_result_dir, self.analyze_obj.output_file_path, job_result.job_name)

        db_services.update_job(self.analyze_obj.db_session, job, JobStatus.FINISHED, "LogL converged")

        # gather occurrences from the jobs, so we don't have to read occurrences again.
        # will not work for parallel jobs. For parallel job, gather will happen in finalize
        if "occurrences" not in gathered_objects:
            gathered_objects["occurrences"] = {}

        for occurrence in job_result.occurrences:
            if occurrence.occurrence_id not in gathered_objects["occurrences"]:
                gathered_objects["occurrences"][occurrence.occurrence_id] = occurrence

        self.analyze_obj.db_session.commit()
        return gathered_objects

    def get_model_stat_fields(self):
        return []


class MESLResultProcessor(DefaultAsremlResultProcessor):
    def create_model_stat_report(self, job_id, asreml_result, job_result, metadata_df):
        # calculate h2 cullis for entry if analysis pattern is SESL

        # write model statisics to analysis report
        rename_keys = {"log_lik": "LogL"}
        model_stat = asreml_result.model_stat

        # modify model_stat to add job and step details
        model_stat["job_id"] = job_id
        model_stat["step"] = job_result.info.get("step") or "1"
        model_stat["genotype_effect"] = job_result.info.get("genotype_effect")
        model_stat["occurrence_name"] = job_result.info.get("occurrence_name")
        h2_cullis = job_result.info.get("h2")

        analysis_report.write_model_stat(
            self.analyze_obj.report_file_path,
            model_stat,
            metadata_df,
            rename_keys,
            h2_cullis=h2_cullis,
            report_fields=self.get_model_stat_fields(),
        )

    def get_model_stat_fields(self):
        return [
            "job_id",
            "step",
            "genotype_effect",
            "occurrence_name",
            "experiment_name",
            "trait_abbreviation",
            "location_name",
            "LogL",
            "aic",
            "bic",
            "components",
            "conclusion",
            "is_converged",
        ]

    # MEML/MESL processor is 1st Entry and only for stage 1
    def create_predictions_reports(self, asreml_result: AsremlRResult, job_result: AsremlRJobResult, metadata_df):
        # create MEML step 1 report
        if "_fixed" in asreml_result.job.name:
            self.create_location_germplasm_report(
                asreml_result.location_germplasm_predictions,
                job_result,
                metadata_df,
                sheet_name=report_constants.ME_ENTRY_SHEET_NAME,
                estimate_type="BLUES",
            )


# class MEMLFirstStepResultProcessor(MESLResultProcessor):

#     # MEML processor is 1st Entry and only for stage 1
#     def create_predictions_reports(self, asreml_result: AsremlRResult, job_result: AsremlRJobResult, metadata_df):
#         # create MEML step 1 report
#         if "_fixed" in asreml_result.job.name:
#             self.create_location_germplasm_report(
#                 asreml_result.location_germplasm_predictions,
#                 job_result,
#                 metadata_df,
#                 sheet_name=analysis_report.ME_ENTRY_SHEET_NAME,
#                 estimate_type="BLUES",
#             )


class MEMLSecondStepResultProcessor(MESLResultProcessor):
    def create_entry_blups_report(self, ge_predictions, metadata_df):
        if ge_predictions is not None:
            analysis_report.create_entry_blups_report(self.analyze_obj.report_file_path, ge_predictions, metadata_df)

    def create_gxe_blups_report(self, gxe_predictions, metadata_df):
        if gxe_predictions is not None:
            analysis_report.create_gxe_blups_report(self.analyze_obj.report_file_path, gxe_predictions, metadata_df)

    def process_job_result(self, job_result: AsremlRJobResult, gathered_objects: dict = None):
        job = db_services.get_job_by_name(self.analyze_obj.db_session, job_result.job_name)

        asreml_result = AsremlRResult(job, job_result.asr_rds_file, job_result.prediction_rds_files)

        if not asreml_result.converged:
            db_services.update_job(
                self.analyze_obj.db_session,
                job,
                JobStatus.FAILED,
                "Failed to converge.",
            )
            return gathered_objects

        # initialize the report workbook
        if not path.isfile(self.analyze_obj.report_file_path):
            utils.create_workbook(self.analyze_obj.report_file_path, sheet_names=report_constants.REPORT_SHEETS)

        metadata_df = utils.get_metadata(job_result.metadata_file)

        # write model_stat
        self.create_model_stat_report(job.id, asreml_result, job_result, metadata_df)

        # write step 2 BLUPS
        self.create_entry_blups_report(asreml_result.germplasm_predictions, metadata_df)

        # write step 2 GxE
        self.create_gxe_blups_report(asreml_result.location_germplasm_predictions, metadata_df)

        utils.zip_dir(job_result.job_result_dir, self.analyze_obj.output_file_path, job_result.job_name)

        db_services.update_job(self.analyze_obj.db_session, job, JobStatus.FINISHED, "LogL converged")

        # # gather occurrences from the jobs, so we don't have to read occurrences again.
        # # will not work for parallel jobs. For parallel job, gather will happen in finalize
        # if "occurrences" not in gathered_objects:
        #     gathered_objects["occurrences"] = {}

        # for occurrence in job_result.occurrences:
        #     if occurrence.occurrence_id not in gathered_objects["occurrences"]:
        #         gathered_objects["occurrences"][occurrence.occurrence_id] = occurrence

        self.analyze_obj.db_session.commit()
        return gathered_objects


class AsremlRAnalyze(AsremlAnalyze):

    engine_script = "asreml-r"
    dpo_cls = AsremlRProcessData

    asr_rds_file_name = "asr.rds"
    prediction_rds_file_name = "prediction{i}.rds"

    def get_job_runner(self, job_data: JobData, analysis_engine, **kwargs) -> JobRunner:
        # get the pattern
        if self.exptloc_analysis_pattern in (SESL, SEML):
            return AsremlRJobRunner(self, job_data, analysis_engine, **kwargs)

        # so if MESL/MEML
        # if job na me has _fixed on it
        if job_data.job_name.endswith("_fixed"):
            return AsremlRMEFixedEffectJobRunner(self, job_data, analysis_engine)

        if job_data.job_name.endswith("_random"):
            return AsremlRMERandomEffectJobRunner(self, job_data, analysis_engine)

    @robjects.packages.no_warnings
    def process_job_result(self, job_result: AsremlRJobResult, gathered_objects: dict = None):
        # TODO: this will need to be refactored
        # introduce JobResultProcessor abstraction

        # check step
        is_first_step = job_result.info.get("step", "1") != "2"

        rproc = None
        if self.exptloc_analysis_pattern == MESL:
            rproc = MESLResultProcessor(self)
        elif self.exptloc_analysis_pattern == MEML:
            if is_first_step:
                rproc = MESLResultProcessor(self)
            else:
                rproc = MEMLSecondStepResultProcessor(self)

        else:
            rproc = DefaultAsremlResultProcessor(self)

        return rproc.process_job_result(job_result, gathered_objects)

    def finalize(self, gathered_objects):
        return super().finalize(gathered_objects)


class AsremlRMemlAnalyze(AsremlRAnalyze):
    dpo_cls = AsremlRMemlProcessData

    def get_job_runner(self, job_data: JobData, analysis_engine, **kwargs) -> JobRunner:
        return AsremlMEMLRandomEffectJobRunner(self, job_data, analysis_engine)
