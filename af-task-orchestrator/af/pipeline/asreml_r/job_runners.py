# Job Runners
import logging

import rpy2
import rpy2.robjects as robjects
from af.pipeline import rpy_utils, utils
from af.pipeline.asreml_r.asreml_r_result import AsremlRResult
from af.pipeline.asreml_r.models import AsremlRJobResult
from af.pipeline.db import services as db_services
from af.pipeline.exceptions import AnalysisError
from af.pipeline.job_runner import JobRunner
from af.pipeline.job_status import JobStatus
from rpy2.robjects.vectors import ListVector

log = logging.getLogger(__name__)


class InvalidFormulaError(ValueError):
    pass


def r_formula(formula: str):
    log.info(f"RFormula: converting {formula}")
    if not formula or not formula.strip():
        return None
    try:
        return robjects.Formula(formula)
    except rpy2.rinterface_lib.embedded.RRuntimeError:
        raise InvalidFormulaError(f"Invalid Formula: {formula}")


class AsremlRJobRunner(JobRunner):
    def get_prediction_parameters(self):
        return {"sed": True}

    def get_run_info(self):
        info = {}

        if "fixed" in self.job_data.job_name:
            info["genotype_effect"] = "fixed"

        if "random" in self.job_data.job_name:
            info["genotype_effect"] = "random"

        info["occurrence_name"] = ",".join([occ.occurrence_name for occ in self.job_data.occurrences])
        return info

    def transform_prediction(self, prediction, *args, **kwargs):
        return prediction, self.get_run_info()

    def get_asreml_run_params(self, r_base, asreml_r):
        return {"na_action": asreml_r.na_method(y="include", x="include")}

    @robjects.packages.no_warnings
    def run_job(self):
        aobj = self.analyze_obj
        session = aobj.db_session
        job_data = self.job_data
        log.info(f"{self.__class__}: Running job = {job_data}")

        job_dir = utils.get_parent_dir(job_data.data_file)

        job_detail = {
            "trait_name": job_data.trait_name,
            "location_name": job_data.location_name,
        }

        job = db_services.create_job(
            session,
            aobj.analysis.id,
            job_data.job_name,
            JobStatus.INPROGRESS,
            "Processing input request",
            job_detail,
        )

        r_base = robjects.packages.importr("base")

        input_data = rpy_utils.read_csv(file=job_data.data_file)

        # set data types to input data fields
        # ASReml-R has only two datatypes, factor & numeric
        # datatype keys are lower case
        datatype_converters = {"factor": r_base.as_factor, "numeric": r_base.as_numeric}
        if job_data.job_params.analysis_fields_types:
            for field_type in job_data.job_params.analysis_fields_types:

                data_type = job_data.job_params.analysis_fields_types.get(field_type)
                converter = datatype_converters.get(data_type)

                if converter:
                    input_data[input_data.colnames.index(field_type)] = converter(input_data.rx2(field_type))

        # asreml has fixed, random and residual formulas.
        model_formulas = {}

        fixed = r_formula(job_data.job_params.fixed)
        if fixed is not None:
            model_formulas["fixed"] = fixed

        random = r_formula(job_data.job_params.random)
        if random is not None:
            model_formulas["random"] = random

        residual = r_formula(job_data.job_params.residual)
        if residual is not None:
            model_formulas["residual"] = residual

        log.info(f"{self.__class__}: Job Formulas {model_formulas}")

        asr_rds_file = utils.path_join(job_dir, aobj.asr_rds_file_name)

        asr = None
        prediction = None

        job_result = AsremlRJobResult(**job_data.__dict__)

        try:

            # license gets checked out when asreml is imported
            asreml_r = robjects.packages.importr("asreml")
            log.info(f"{self.__class__}: Running job -- imported asremlr")
            # build na_action
            log.info(f"Asreml run params: {self.get_asreml_run_params(r_base, asreml_r)}")

            asr = asreml_r.asreml(**model_formulas, data=input_data, **self.get_asreml_run_params(r_base, asreml_r))

            if asr:
                log.info(f"{self.__class__}: AsremlRAnalyze: asr is ok")

                # try to converge by updating asr
                tries = 0
                while not AsremlRResult.is_converged(asr) and tries < self.get_convergence_tries():
                    asr = asreml_r.update_asreml(asr, **model_formulas)
                    tries += 1

                # save asr as rds file
                r_base.saveRDS(asr, asr_rds_file)
                log.info(f"{self.__class__}: saved asr rds file: {asr_rds_file}")

                job_result.asr_rds_file = asr_rds_file

                # run predictions
                for i, prediction_statement in enumerate(job_data.job_params.predictions):
                    prediction = asreml_r.predict_asreml(
                        object=asr, classify=prediction_statement, **self.get_prediction_parameters(), **model_formulas
                    )

                    prediction, info = self.transform_prediction(
                        prediction,
                        prediction_statement=prediction_statement,
                        asr=asr,
                        input_data=input_data,
                    )

                    job_result.info.update(info)

                    prediction_rds_file = utils.path_join(job_dir, aobj.prediction_rds_file_name.format(i=i + 1))
                    r_base.saveRDS(prediction, prediction_rds_file)
                    log.info(f"{self.__class__}: saved for pred: {prediction_statement} - {prediction_rds_file}")

                    job_result.prediction_rds_files.append(prediction_rds_file)

            # checking in the license back
            r_base.detach("package:asreml", unload=True)

        except (rpy2.rinterface_lib.embedded.RRuntimeError, ValueError) as e:
            log.error(f"{self.__class__}: Encountered run job error -  {str(e)}")
            aobj.analysis.status = "FAILURE"
            db_services.update_job(session, job, JobStatus.ERROR, str(e))
            utils.zip_dir(job_dir, aobj.output_file_path, job.name)
            raise AnalysisError("Model is not yet supported.")
            # TODO: Will need to be more specific with the errors

        job_result.job_result_dir = job_dir

        return job_result


class AsremlRMEFixedEffectJobRunner(AsremlRJobRunner):
    def get_prediction_parameters(self):
        return {"vcov": True}

    @robjects.packages.no_warnings
    def transform_prediction(self, prediction, *args, **kwargs):
        # ME Fixed should compute for the weights
        # predictions is an rbase df, also passed in the r_base object here
        r_base = robjects.packages.importr("base")
        utils = robjects.packages.importr("utils")
        error_func_code = "function(e) { wts <- NA }"
        error_func = robjects.r(error_func_code)
        weights = r_base.tryCatch(r_base.diag(r_base.solve(prediction[1])), error=error_func)

        prediction[0] = r_base.cbind(prediction[0], weight=weights)

        # add loc
        # read the loc and expt columns in input_data
        input_data = kwargs.get("input_data")  # type: rpy2.robjects.vectors.DataFrame
        pvals = prediction.rx2("pvals")
        expt_loc = input_data.rx(r_base.c("expt", "loc"))
        expt_loc = utils.head(expt_loc, pvals.nrow)

        prediction[0] = r_base.cbind(pvals, expt_loc)

        return prediction, self.get_run_info()


class AsremlRMERandomEffectJobRunner(AsremlRJobRunner):
    @robjects.packages.no_warnings
    def transform_prediction(self, prediction, *args, **kwargs):
        log.info(f"{self.__class__}: Computing for H2 value")
        # ME Fixed should compute for H2 Cullis
        r_base = robjects.packages.importr("base")
        asr = kwargs.get("asr")
        pred_stmt = kwargs.get("prediction_statement")

        summary = r_base.summary(asr, all=True)

        # get the varcomp
        varcomp = summary.rx2("varcomp")
        # get the pred component
        pred_component = varcomp.rx(pred_stmt, "component")

        matrixlib = robjects.packages.importr("Matrix")
        # get the sed from prediction
        sed = prediction.rx2("sed")
        matrix = matrixlib.as_matrix(sed)
        row, col = matrix.dim
        sqrfunc = robjects.r("function(x) x ^ 2")
        # square sed values
        sqrd_mat = r_base.sapply(matrix, sqrfunc)
        sqrd_mat = robjects.r.matrix(sqrd_mat, nrow=row, ncol=col)
        upper_tri = r_base.upper_tri(matrix, diag=False)
        # filter upper tri values and compute mean
        values = sqrd_mat.rx(upper_tri)
        mean = r_base.mean(values, na_rm=True)

        h2 = max(0, 1 - mean[0] / (2 * pred_component[0]))
        log.info(f"{self.__class__}: Computing for H2 value DONE: {h2}")
        info = self.get_run_info()
        info["h2"] = h2
        # save the h2 in prediction
        new_entry = ListVector(info)
        prediction = r_base.append(prediction, new_entry)
        log.info(f"{self.__class__}: Added h2 entry on prediction object")

        return prediction, info


class AsremlMEMLRandomEffectJobRunner(AsremlRMERandomEffectJobRunner):
    def get_convergence_tries(self) -> int:
        return 4

    def get_asreml_run_params(self, r_base, asreml_r):
        return {
            "na_action": asreml_r.na_method(y="omit", x="omit"),
            "weights": r_base.c("weight"),
            "method": "em",
        }
