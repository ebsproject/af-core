from dataclasses import dataclass, field

from af.pipeline.job_data import JobData


@dataclass
class AsremlRJobResult(JobData):
    asr_rds_file: str = ""
    prediction_rds_files: list[str] = field(default_factory=list)
    info: dict = field(default_factory=dict)
