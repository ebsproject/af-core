import collections
import logging
import os
import shlex

import pandas as pd
from af.pipeline import config, pandasutil, utils
from af.pipeline.asreml.dpo import AsremlProcessData
from af.pipeline.db import services
from af.pipeline.exceptions import AnalysisError, DpoException
from af.pipeline.job_data import JobData, JobParams
from af.pipeline.utils import convert
from rpy2 import robjects

log = logging.getLogger(__name__)


class AsremlRProcessData(AsremlProcessData):
    def mesl(self):
        log.info("AsremlRProcessData: Running mesl()")
        log.info(f"AsremlRProcessData: Location ids: {self.location_ids}")
        log.info(f"AsremlRProcessData: Traits: {self.trait_ids}")
        return self._generate_jobs()

    def _generate_jobs(self):
        jobs = []

        data_by_location_trait = collections.defaultdict(pd.DataFrame)
        metadata_by_location_trait = collections.defaultdict(pd.DataFrame)
        occurrence_lookup = {}

        for data, metadata, occurrence, trait in self.__get_analysis_data():

            location_trait = (str(occurrence.location_id), str(trait.trait_id))
            data_by_location_trait[location_trait] = data_by_location_trait[location_trait].append(data)
            metadata_by_location_trait[location_trait] = metadata_by_location_trait[location_trait].append(metadata)
            occurrence_lookup[location_trait] = occurrence

        for location_id in self.location_ids:
            for trait_id in self.trait_ids:
                log.info(f"AsremlRProcessData: processing location id: {location_id}, trait: {trait_id}")
                data = data_by_location_trait[(location_id, trait_id)]
                metadata = metadata_by_location_trait[(location_id, trait_id)]
                occurrence = occurrence_lookup[(location_id, trait_id)]
                jobs.extend(
                    self.__generate_me_job_objects(
                        data, occurrence=occurrence, metadata=metadata, location_id=location_id, trait_id=trait_id
                    )
                )

        return jobs

    def meml(self):
        log.info("AsremlRProcessData: Running meml()")
        log.info(f"AsremlRProcessData: Location ids: {self.location_ids}")
        log.info(f"AsremlRProcessData: Traits: {self.trait_ids}")

        return self._generate_jobs()

    def __get_analysis_data(self):
        """Use the data readers to get analysis data"""
        for occurr_id in self.occurrence_ids:
            log.info(f"AsremlRProcessData: Getting plots for occurence {occurr_id}")
            plots = self.data_reader.get_plots(occurrence_id=occurr_id)
            occurrence = self.data_reader.get_occurrence(occurr_id)

            for trait_id in self.trait_ids:
                log.info(f"AsremlRProcessData: Getting plots for occurence {occurr_id}, trait: {trait_id}")
                trait = self.get_trait_by_id(trait_id)
                plot_measurements = self.data_reader.get_plot_measurements(occurrence_id=occurr_id, trait_id=trait_id)
                data = plots.merge(plot_measurements, on="plot_id", how="left")
                data = pandasutil.add_obs_count(data, "germplasmDbId", "trait_value")
                metadata = self._generate_me_metadata(data, occurrence, trait)
                yield data, metadata, occurrence, trait

    def _generate_me_metadata(self, plots, occurrence, trait):

        metadata_columns = ["entry_id", "entry_name", "entry_type", "germplasmDbId", "obs_count"]
        metadata = plots.loc[:, metadata_columns]  # get a copy, not a view
        metadata["experiment_id"] = occurrence.experiment_id
        metadata["experiment_name"] = occurrence.experiment_name
        metadata["location_name"] = occurrence.location
        metadata["location_id"] = occurrence.location_id
        metadata["trait_abbreviation"] = trait.abbreviation
        metadata["data_type"] = trait.data_type

        # entry id is coming out as float so -- BA2-228
        if "entry_id" in metadata:
            metadata["entry_id"] = metadata["entry_id"].map(lambda x: convert(x))

        return metadata

    # -- for SE methods
    def __generate_job_object(self, job_name, data, metadata, trait_id):
        job = JobData(job_name=job_name)
        job.metadata_file = self.save_metadata(job_name, metadata)
        trait = self.get_trait_by_id(trait_id)
        data = self._format_result_data(data, trait)
        self._write_job_data(job, data, trait)

        return job

    def _set_job_params(self, job_data, trait):

        formula = self.__parse_formula(self._get_formula(trait))
        job_params = JobParams(**formula, residual=self._get_residual(), predictions=[])

        # set predictions statements
        predictions = self._get_predictions()
        if not predictions:
            raise ValueError("Predictions are not available for the job to process")

        job_params.predictions = [prediction.statement for prediction in predictions]

        # map analysis fields to their data type
        analysis_fields = self._get_analysis_fields()
        job_params.analysis_fields_types = {}
        for field in analysis_fields:
            job_params.analysis_fields_types[field.Property.code] = field.Property.data_type.lower()

        job_data.job_params = job_params

    # -- for ME methods

    def __generate_me_job_objects(self, data, occurrence, metadata, location_id, trait_id, job_type=config.MESL):
        """Since the set is run with fixed and random models concurrently,
        we will affix _fixed and _random in the job names
        """
        log.info("AsremlRProcessData: Generating ME Job Objects")
        me_jobs = []
        trait = self.get_trait_by_id(trait_id)
        log.info(f"AsremlRProcessData: Raw data: {data}")
        log.info(f"AsremlRProcessData: Detected raw columns: {data.columns}")

        field_coord = "with" if "pa_x" in data and "pa_y" in data else "without"
        log.info(f"AsremlRProcessData: Detecting field-coord: {field_coord}")

        # step should be 1
        step = "1"

        # get the configs
        design = occurrence.experiment_design_type
        analysis_configs = services.get_property_by_meta(
            self.db_session, design=design, step=step, field_coord=field_coord
        )

        # there should be two always, one for genotype as fixed and
        # one for genotype as random
        for analysis_config in analysis_configs:
            # get info on analysis field here so just pass it along
            analysis_fields = services.get_analysis_config_module_fields(self.db_session, analysis_config.id)

            # generate the job for this analysis_config
            # get the genotype effect value first
            data_copy = data.copy(deep=True)
            log.info(
                f"AsremlRProcessData: Adding Job with Analysis Config Id {analysis_config.id} - {analysis_config.code}"
            )
            formatted_data = self._format_me_job_result_data(analysis_config.id, data_copy, trait, analysis_fields)

            log.info(f"AsremlRProcessData: Formatted data: {formatted_data}")
            log.info(f"AsremlRProcessData: Detected formatted columns: {formatted_data.columns}")
            genotype_effect = analysis_config.get_meta_value("genotype_effect")

            job_name = self.__get_me_job_name(location_id, trait_id, effect=genotype_effect)

            me_jobs.append(
                self.__generate_me_job_object(
                    job_name, occurrence, analysis_config, formatted_data, metadata, trait, analysis_fields
                )
            )
        log.info(f"AsremlRProcessData: Generated {len(me_jobs)} jobs")
        return me_jobs

    def _get_me_job_input_fields_config_fields(self, analysis_fields):
        """Map of input data fields to analysis configuration fields."""

        input_fields_to_config_fields = collections.OrderedDict()

        for field in analysis_fields:
            input_field_name = field.property_meta.get("definition")

            if input_field_name is None:
                raise DpoException("Analysis config fields have no definition")

            input_fields_to_config_fields[input_field_name] = field.Property.code

        return input_fields_to_config_fields

    def _format_me_job_result_data(self, analysis_config_id, plots_and_measurements, trait, analysis_fields):

        input_fields_to_config_fields = self._get_me_job_input_fields_config_fields(analysis_fields)
        log.info(f"AsremlRProcessData: Config fields: {input_fields_to_config_fields}")

        # drop trait id
        plots_and_measurements.drop(["trait_id"], axis=1, inplace=True, errors="ignore")

        trait_qc = plots_and_measurements.trait_qc

        # rename
        plots_and_measurements.loc[trait_qc == "B", "trait_value"] = "NA"

        # map trait value column to trait name
        abbrev = trait.abbreviation
        input_fields_to_config_fields["trait_value"] = abbrev

        # Key only the config field columns
        plots_and_measurements = pandasutil.df_keep_columns(
            plots_and_measurements, input_fields_to_config_fields.keys()
        )

        plots_and_measurements = plots_and_measurements.rename(columns=input_fields_to_config_fields)
        plots_and_measurements = plots_and_measurements[input_fields_to_config_fields.values()]

        # fill trait value with NA string
        plots_and_measurements[[abbrev]] = plots_and_measurements[[abbrev]].fillna(config.UNIVERSAL_UNKNOWN)

        return plots_and_measurements

    def __generate_me_job_object(
        self, job_name, occurrence, analysis_config, plots_data, metadata, trait, analysis_fields
    ):
        job = JobData(job_name=job_name)

        job.metadata_file = self.save_metadata(job_name, metadata)

        # set up the trait name
        job.trait_name = trait.abbreviation

        job.occurrences = [occurrence]

        # BA2-233
        job.trait_name = trait.abbreviation
        if len(job.occurrences) > 1:
            job.location_name = "Multi Location"
        else:
            job.location_name = job.occurrences[0].location

        # setup the data files
        data_file_name = f"{job.job_name}.csv"
        job.job_result_dir = self.get_job_folder(job.job_name)
        job.data_file = os.path.join(job.job_result_dir, data_file_name)

        # by default sort by columns 'row' and 'col'. row and col here denotes plot's row and column
        sort_cols = []
        if "loc" in plots_data:
            sort_cols = ["loc"]
        if "row" in plots_data and "col" in plots_data:
            sort_cols.extend(["row", "col"])
            plots_data.row = pd.to_numeric(plots_data.row, errors="coerce")
            plots_data.col = pd.to_numeric(plots_data.col, errors="coerce")
            plots_data = plots_data.sort_values(by=sort_cols)

        plots_data.to_csv(job.data_file, index=False)

        # setup the job_params/formulas
        self.set_me_job_params(job, analysis_config, trait, analysis_fields)
        return job

    def __get_me_job_name(self, location_id, trait_id, effect="fixed"):
        exploc = str(self.exptloc_analysis_pattern).lower()
        return f"{self.analysis_request.requestId}_{exploc}_{location_id}_{trait_id}_{effect}"

    def __get_mesl_job_name(self, location_id, trait_id, effect="fixed"):
        return f"{self.analysis_request.requestId}_mesl_{location_id}_{trait_id}_{effect}"

    def __get_meml_job_name(self, trait_id, model="fixed"):
        return f"{self.analysis_request.requestId}_meml_{trait_id}_{model}"

    def __parse_formula(self, formula):
        if not formula or not formula.strip():
            return {}

        lexer = shlex.shlex(formula)

        lexer.whitespace_split = True
        lexer.whitespace = ","

        params = dict([s.strip() for s in pair.split("=", 1)] for pair in lexer)

        return params

    def set_me_job_params(self, job, analysis_config, trait, analysis_fields):
        # get the formulas
        formula_items = services.get_analysis_config_properties(self.db_session, analysis_config.id, "formula")
        formula_stmt = formula_items[0].statement.format(trait_name=trait.abbreviation)
        formulas = self.__parse_formula(formula_stmt)

        residual_items = services.get_analysis_config_properties(self.db_session, analysis_config.id, "residual")
        residual_stmt = residual_items[0].statement

        job_params = JobParams(**formulas, residual=residual_stmt, predictions=[])

        # set predictions statements
        predictions = self._get_predictions_for_analysis_config(analysis_config.id)

        if not predictions:
            raise ValueError("Predictions are not available for the job to process")

        job_params.predictions = [prediction.statement for prediction in predictions]

        # map analysis fields to their data type
        job_params.analysis_fields_types = {}
        for field in analysis_fields:
            job_params.analysis_fields_types[field.Property.code] = field.Property.data_type.lower()

        job.job_params = job_params

    def _get_predictions_for_analysis_config(self, analysis_config_id):

        predictions = []

        if not len(self.analysis_request.configPredictionPropertyIds):
            predictions = services.get_analysis_config_properties(self.db_session, analysis_config_id, "prediction")
        else:
            for prediction_property_id in self.analysis_request.configPredictionPropertyIds:
                predictions.append(services.get_property(self.db_session, prediction_property_id))

        return predictions


class AsremlRMemlProcessData(AsremlRProcessData):
    def run(self, *args, **kwargs):
        """Override run from parent to ignore expt_loc methods"""
        log.info(f"{self.__class__}: Running second stage analysis DPO step - collating data")
        # this just takes the step 1 results to build job
        jobs = []
        step_1_results = kwargs.get("results")  # type: list

        # group jobs by trait
        jobs_per_trait = {}

        for result in step_1_results:
            # parse job_name for the trait_id
            # should requestId_meml_locid_traitid_geno
            (_, _, _, traitId, _) = str(result.job_name).split("_")

            if traitId in jobs_per_trait:
                jobs_per_trait[traitId].append(result)
                continue
            jobs_per_trait[traitId] = [result]

        # run each trait
        for trait_id, joblist in jobs_per_trait.items():
            jobs.extend(self.__generate_meml_job(trait_id, joblist))

        return jobs

    def __generate_meml_job(self, trait_id, jobs):

        # get random job runs with h2 value >= 0.1
        random_geno_jobs = [
            res for res in jobs if str(res.job_name).endswith("random") and "h2" in res.info and res.info["h2"] >= 0.1
        ]

        if not random_geno_jobs:
            raise AnalysisError("No viable data set for stage 2 analysis")

        # get corresponding fixed geno jobs
        fixed_geno_jobs = []
        for random_job in random_geno_jobs:
            fixed_job_name = str(random_job.job_name).replace("random", "fixed")
            job_select = [res for res in jobs if res.job_name == fixed_job_name]
            fixed_geno_jobs.extend(job_select)

        data_file_contents = self.__generate_data_file_contents(fixed_geno_jobs)

        # TODO: combine metadata files of selected jobs
        metadata_contents = self.__generate_meml_metadata(fixed_geno_jobs)

        # step should be 2
        step = "2"

        trait = self.get_trait_by_id(trait_id)
        # get the configs
        analysis_configs = services.get_property_by_meta(
            self.db_session, step=step  # TODO: modify with additional params later on
        )  # configs
        # hacky way to write CSV in rpy2
        robjects.globalenv["data_file"] = data_file_contents

        # convert the r data frame to pandas
        # pd_dt = robjects.conversion.rpy2py(data_file_contents)
        # pd_dt = pandasutil.add_obs_count(pd_dt, 'ge', 'predicted_value')
        step_2_jobs = []
        for analysis_config in analysis_configs:  # type: Property
            # get info on analysis field here so just pass it along
            analysis_fields = services.get_analysis_config_module_fields(self.db_session, analysis_config.id)

            # generate the job for this analysis_config
            # get the genotype effect value first

            log.info(
                f"{self.__class__}: Adding Job with Analysis Config Id {analysis_config.id} - {analysis_config.code}"
            )

            # genotype_effect = analysis_config.get_meta_value("genotype_effect")
            meml_job = JobData()
            # get genotype_effect meta data
            effect = analysis_config.get_meta_value("genotype_effect")

            job_name = f"{self.analysis_request.requestId}_meml_{trait_id}_step2_{effect}_{analysis_config.id}"
            meml_job.job_name = job_name
            data_file_name = f"{meml_job.job_name}.csv"
            meml_job.job_result_dir = self.get_job_folder(meml_job.job_name)
            meml_job.data_file = os.path.join(meml_job.job_result_dir, data_file_name)

            # write metadata
            meml_job.metadata_file = self.save_metadata(job_name, metadata_contents)

            robjects.r(f"write.csv(data_file, file='{meml_job.data_file}', row.names=FALSE)")
            # pd_dt.to_csv(meml_job.data_file, index=False)

            self.set_me_job_params(meml_job, analysis_config, trait, analysis_fields)
            # job_params
            step_2_jobs.append(meml_job)
        # generate
        # TODO: select the  analysis config for the job_params
        return step_2_jobs

    def __generate_meml_metadata(self, jobs):
        meta_dfs = []
        for job in jobs:
            metadata_file = self.get_meta_data_file_path(job.job_name)
            df = utils.get_metadata(metadata_file)
            meta_dfs.append(df)

        metadata = pd.concat(meta_dfs)
        return metadata

    @robjects.packages.no_warnings
    def __generate_data_file_contents(self, jobs):

        # setup the data files

        r_base = robjects.packages.importr("base")
        merged_pvals = None

        for job in jobs:
            for pred_file in job.prediction_rds_files:
                data = r_base.readRDS(pred_file)
                pvals = data.rx2("pvals")
                if not merged_pvals:
                    merged_pvals = pvals
                else:
                    merged_pvals = r_base.rbind(merged_pvals, pvals)

        # rename columns
        cols = r_base.colnames(merged_pvals)
        if cols.index("predicted.value") >= 0:
            cols[cols.index("predicted.value")] = "predicted_value"

        if cols.index("std.error") >= 0:
            cols[cols.index("std.error")] = "std_error"

        # drop status column
        status_index = cols.index("status") + 1
        columns = []
        for i in range(1, len(cols) + 1):
            if i != status_index:
                columns.append(i)

        int_vec = robjects.IntVector(tuple(columns))

        merged_pvals = merged_pvals.rx(True, int_vec)

        return merged_pvals
