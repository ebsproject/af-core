REQUEST_INFO_SHEET_NAME = "Request Info"
ENTRY_SHEET_NAME = "Entry"
ME_ENTRY_SHEET_NAME = "1st Step (Entry)"
LOCATION_SHEET_NAME = "Location"
ENTRY_LOCATION_SHEET_NAME = "Entry x Location"
MODEL_STAT_SHEET_NAME = "Model Statistics"
BLUPS_SHEET_NAME = "2nd Step (Entry) - BLUPS"
GXE_BLUPS_SHEET_NAME = "2nd Step (GxE) - BLUPS"


REPORT_SHEETS = [
    REQUEST_INFO_SHEET_NAME,
    MODEL_STAT_SHEET_NAME,
    ENTRY_SHEET_NAME,
    LOCATION_SHEET_NAME,
    ENTRY_LOCATION_SHEET_NAME,
    BLUPS_SHEET_NAME,
    GXE_BLUPS_SHEET_NAME,
]

DEFAULT_MODEL_STAT_COLUMNS = [
    "job_id",
    "experiment_name",
    "trait_abbreviation",
    "data_type",
    "location_name",
    "LogL",
    "aic",
    "bic",
    "components",
    "conclusion",
    "is_converged",
    "h2_cullis",
    "notes",
]
