import logging

import pandas as pd
from af.pipeline import pandasutil, report_constants
from af.pipeline.db import services as db_services

log = logging.getLogger(__name__)


def write_request_settings(db_session, report_file, analysis_request):

    # write request settings report
    formula = db_services.get_property(db_session, analysis_request.configFormulaPropertyId)
    formula_name = "" if not formula else formula.name
    residual = db_services.get_property(db_session, analysis_request.configResidualPropertyId)
    residual_name = "" if not residual else residual.name
    prediction = db_services.get_property(db_session, analysis_request.analysisObjectivePropertyId)
    config = db_services.get_property(db_session, analysis_request.analysisConfigPropertyId)
    config_name = "" if not config else config.name
    exptloc_analysis_pattern = db_services.get_property(db_session, analysis_request.expLocAnalysisPatternPropertyId)

    # TODO : How will ME request settings reports be written?

    request_settings_build = [
        {"Request Settings": ""},
        {"": ""},
        {"Request Settings": "Objective", "": prediction.name},
        {"Request Settings": "Trait Pattern", "": "Univariate"},
        {"Request Settings": "Experiment Location Pattern", "": exptloc_analysis_pattern.name},
        {"Request Settings": "Analysis Configuration", "": config_name},
        {"Request Settings": "Main Model", "": formula_name},
        {"Request Settings": "Spatial Adjustment", "": residual_name},
    ]

    request_settings = pd.DataFrame(request_settings_build)

    pandasutil.append_df_to_excel(report_file, request_settings, sheet_name=report_constants.REQUEST_INFO_SHEET_NAME)


def write_occurrences(report_file, occurrences):
    log.info(f"Writing down occurrences in report: {len(occurrences)}")
    # write a gap for occurrence
    gaps = pd.DataFrame([{"": ""}, {"": ""}])
    pandasutil.append_df_to_excel(report_file, gaps, sheet_name=report_constants.REQUEST_INFO_SHEET_NAME)

    # write occurrences
    occurrence_report_build = []  # empty dict for occurrences
    for occurrence_id in occurrences:
        occurrence_report_build.append(occurrences[occurrence_id].dict())

    occurrence_report = pd.DataFrame(occurrence_report_build)
    pandasutil.append_df_to_excel(
        report_file, occurrence_report, sheet_name=report_constants.REQUEST_INFO_SHEET_NAME, header=True
    )


def write_predictions(
    db_session, analysis_request, report_file: str, predictions_df: pd.DataFrame, metadata_df: pd.DataFrame
):
    """Writes spearate report sheet for predictions of entry, location and entry x location in
    the report workbook.
    Args:
        report_file: file path of the report to whcih predictions will be written.
        predictions: Dataframe with following columns (* - indicates mandatory columns)
            job_id*: Id of the job which generated the predictions.
            entry: Id of the entry, if prediction is for entry
            loc: Id of the location, if the prediction is for location
            value*: Value of the prediction
            std_err*: Standard error for prediction
            num_factors*: Number of factors used by prediction
        metadata_df: DataFrame for metadata
            Index: RangeIndex
            Columns:
                entry_id: Id of the entry
                entry_name: Name of the entry
                entry_type: Type of the entry (example values: test, check)
                experiment_id: Id of the experiment
                experiment_name: Name of the experiment
                location: Name of the location
                location_id: Id of the location
                trait_abbreviation: Abbreviation of trait name
    """

    if len(predictions_df) == 0:
        return
    log.info(f"Report Writer: {len(predictions_df)} items in frame")
    # write entry report
    if "entry" in predictions_df.columns:
        log.info("Report Writer: 'entry' in frame -- writing entry predictions")
        entry_report = predictions_df[predictions_df.entry.notnull()]
        entry_report = entry_report[entry_report.num_factors == 1]
        write_entry_predictions(db_session, analysis_request, report_file, entry_report, metadata_df)

    # write location report
    if "loc" in predictions_df.columns:
        # get location only rows
        # do not try to call loc column as property as it would conflict with default loc property
        log.info("Report Writer: 'loc' in frame -- writing loc predictions")
        location_report = predictions_df[predictions_df["loc"].notnull()]
        location_report = location_report[location_report.num_factors == 1]
        write_location_predictions(report_file, location_report, metadata_df)

    # write entry and location report
    if "entry" in predictions_df.columns and "loc" in predictions_df.columns:
        # get entry location only rows
        log.info("Report Writer: 'entry' and 'loc' in frame -- writing entry-loc predictions")
        entry_location_report = predictions_df[predictions_df.entry.notnull()]
        entry_location_report = entry_location_report[entry_location_report["loc"].notnull()]
        entry_location_report = entry_location_report[entry_location_report.num_factors == 2]
        write_entry_location_predictions(report_file, entry_location_report, metadata_df)


def write_entry_predictions(
    db_session, analysis_request, report_file: str, entry_report: pd.DataFrame, metadata_df: pd.DataFrame
):

    entry_report_columns = [
        "job_id",
        "experiment_id",
        "experiment_name",
        "trait_abbreviation",
        "entry_id",
        "entry_name",
        "entry_type",
        "obs_count",
        "value",
        "std_error",
    ]

    exptloc_analysis_pattern = db_services.get_property(db_session, analysis_request.expLocAnalysisPatternPropertyId)

    if exptloc_analysis_pattern.code == "SESL":
        pos_to_insert_loc_cols = entry_report_columns.index("experiment_name") + 1
        entry_report_columns[pos_to_insert_loc_cols:pos_to_insert_loc_cols] = ["location_id", "location_name"]

    if len(entry_report) == 0:
        return
    entry_report["entry"] = entry_report["entry"].astype(str)
    entry_report = entry_report.merge(metadata_df, left_on="entry", right_on="entry_id")
    entry_report = entry_report[entry_report_columns]
    entry_report = entry_report.drop_duplicates()

    pandasutil.set_columns_as_numeric(entry_report, ["value", "std_error"])
    pandasutil.append_df_to_excel(report_file, entry_report, sheet_name=report_constants.ENTRY_SHEET_NAME)


def write_location_germplasm_predictions(
    db_session,
    analysis_request,
    report_file: str,
    location_germplasm_report: pd.DataFrame,
    metadata_df: pd.DataFrame,
    sheet_name=report_constants.ENTRY_SHEET_NAME,
    estimate_type="BLUEs",
):

    report_columns = [
        "job_id",
        "experiment_id",
        "experiment_name",
        "location_id",
        "location_name",
        "trait_abbreviation",
        "entry_id",
        "entry_name",
        "entry_type",
        "germplasm_id",
        "obs_count",
        "estimate_type",
        "value",
        "std_error",
        "weight",
    ]

    if len(location_germplasm_report) == 0:
        return

    location_germplasm_report["ge"] = location_germplasm_report["ge"].astype(str)
    location_germplasm_report.rename(columns={"ge": "germplasm_id"}, inplace=True)
    location_germplasm_report = location_germplasm_report.merge(
        metadata_df, left_on="germplasm_id", right_on="germplasmDbId"
    )

    # add estimate type column
    location_germplasm_report["estimate_type"] = estimate_type

    location_germplasm_report = location_germplasm_report[report_columns]
    location_germplasm_report = location_germplasm_report.drop_duplicates()

    pandasutil.set_columns_as_numeric(location_germplasm_report, ["value", "std_error"])
    pandasutil.append_df_to_excel(report_file, location_germplasm_report, sheet_name=sheet_name)


def write_location_predictions(report_file: str, location_report: pd.DataFrame, metadata_df: pd.DataFrame):
    location_report_columns = ["job_id", "trait_abbreviation", "location_id", "location_name", "value", "std_error"]
    location_df = metadata_df[["location_id", "location_name", "trait_abbreviation"]].drop_duplicates()

    if len(location_report) == 0:
        return

    location_report = location_report.merge(location_df, left_on="loc", right_on="location_id")
    location_report = location_report[location_report_columns]
    location_report = location_report.drop_duplicates()

    pandasutil.set_columns_as_numeric(location_report, ["value", "std_error"])
    pandasutil.append_df_to_excel(report_file, location_report, sheet_name=report_constants.LOCATION_SHEET_NAME)


def write_entry_location_predictions(report_file: str, entry_location_report: pd.DataFrame, metadata_df: pd.DataFrame):

    entry_location_report_columns = [
        "job_id",
        "trait_abbreviation",
        "entry_id",
        "entry_name",
        "location_id",
        "location_name",
        "obs_count",
        "value",
        "std_error",
    ]

    if len(entry_location_report) == 0:
        return

    entry_location_report["entry"] = entry_location_report["entry"].astype(str)

    entry_location_report = entry_location_report.merge(
        metadata_df, left_on=["entry", "loc"], right_on=["entry_id", "location_id"]
    )

    entry_location_report = entry_location_report[entry_location_report_columns]
    entry_location_report = entry_location_report.drop_duplicates()

    pandasutil.set_columns_as_numeric(entry_location_report, ["value", "std_error"])
    pandasutil.append_df_to_excel(
        report_file, entry_location_report, sheet_name=report_constants.ENTRY_LOCATION_SHEET_NAME
    )


def write_model_stat(
    report_file: str, model_stat: dict, metadata_df: pd.DataFrame, rename_map: dict, h2_cullis=None, report_fields=None
):

    # write model statistics
    if len(model_stat) == 0:
        return

    model_stats_report_columns = report_fields or list(report_constants.DEFAULT_MODEL_STAT_COLUMNS)

    if h2_cullis is not None:
        model_stat["h2_cullis"] = h2_cullis
    else:
        try:
            model_stats_report_columns.remove("h2_cullis")
        except ValueError:
            pass

    model_stats_df = pd.DataFrame([model_stat])

    model_stats_df["experiment_name"] = ",".join(metadata_df.experiment_name.drop_duplicates().astype(str))
    model_stats_df["location_name"] = ",".join(metadata_df.location_name.drop_duplicates().astype(str))
    model_stats_df["trait_abbreviation"] = ",".join(metadata_df.trait_abbreviation.drop_duplicates().astype(str))
    model_stats_df["data_type"] = ",".join(metadata_df.data_type.drop_duplicates().astype(str))

    model_stats_df["notes"] = model_stats_df.apply(
        lambda row: "Non-normality not considered" if "integer" in str(row["data_type"]).lower() else "",
        axis=1,
    )

    model_stats_df = model_stats_df.rename(columns=rename_map)
    model_stats_df = pandasutil.df_keep_columns(model_stats_df, model_stats_report_columns)

    pandasutil.set_columns_as_numeric(model_stats_df, ["LogL", "aic", "bic"])
    pandasutil.append_df_to_excel(report_file, model_stats_df, sheet_name=report_constants.MODEL_STAT_SHEET_NAME)


def create_entry_blups_report(report_file: str, blups_report: pd.DataFrame, metadata_df: pd.DataFrame):
    blups_report_columns = [
        "job_id",
        "experiment_id",
        "experiment_name",
        "trait_abbreviation",
        "entry_id",
        "entry_name",
        "germplasm_id",
        "obs_count",
        "estimate_type",
        "value",
        "std_error",
    ]

    if len(blups_report) == 0:
        return

    blups_report = blups_report.merge(metadata_df, left_on=["ge"], right_on=["germplasmDbId"])

    blups_report["ge"] = blups_report["ge"].astype(str)
    blups_report.rename(columns={"ge": "germplasm_id"}, inplace=True)
    # add estimate type column
    blups_report["estimate_type"] = "BLUP"
    blups_report = blups_report[blups_report_columns]
    blups_report = blups_report.drop_duplicates()

    pandasutil.set_columns_as_numeric(blups_report, ["value", "std_error"])
    pandasutil.append_df_to_excel(report_file, blups_report, sheet_name=report_constants.BLUPS_SHEET_NAME)


def create_gxe_blups_report(report_file: str, blups_report: pd.DataFrame, metadata_df: pd.DataFrame):
    blups_report_columns = [
        "job_id",
        "experiment_id",
        "experiment_name",
        "location_id",
        "location_name",
        "trait_abbreviation",
        "entry_id",
        "entry_name",
        "germplasm_id",
        "obs_count",
        "estimate_type",
        "value",
        "std_error",
    ]

    if len(blups_report) == 0:
        return

    blups_report = blups_report.merge(metadata_df, left_on=["ge", "loc"], right_on=["germplasmDbId", "location_id"])

    blups_report["ge"] = blups_report["ge"].astype(str)
    blups_report.rename(columns={"ge": "germplasm_id"}, inplace=True)
    # add estimate type column
    blups_report["estimate_type"] = "BLUPS"
    blups_report = blups_report[blups_report_columns]
    blups_report = blups_report.drop_duplicates()

    pandasutil.set_columns_as_numeric(blups_report, ["value", "std_error"])
    pandasutil.append_df_to_excel(report_file, blups_report, sheet_name=report_constants.GXE_BLUPS_SHEET_NAME)
