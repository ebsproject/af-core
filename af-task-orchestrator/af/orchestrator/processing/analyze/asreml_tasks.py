import gc
import logging

from af.orchestrator.app import app
from af.orchestrator.base import StatusReportingTask
from af.pipeline import analyze as pipeline_analyze
from af.pipeline.asreml_r.analyze import AsremlRMemlAnalyze
from af.pipeline.config import MEML
from af.pipeline.exceptions import AnalysisError

log = logging.getLogger(__name__)


@app.task(name="run_asreml_analyze", base=StatusReportingTask, queue="ASREML")
def run_asreml_analyze(*args):
    _run_analyze(*args)


@app.task(name="run_analyze", base=StatusReportingTask)
def run_analyze(*args):
    _run_analyze(*args)


@app.task(name="run_second_stage_analysis", base=StatusReportingTask, queue="ASREML")
def run_second_stage_analysis(request_id, analysis_request, results):
    # TODO: Refactor / design this as an Analyze object
    # step 1
    # get the job_results and separate into fixed -- random

    analyze_obj = AsremlRMemlAnalyze(analysis_request=analysis_request)

    input_files = analyze_obj.pre_process(results=results)
    second_stage_results = []
    for input_file in input_files:
        try:
            result = analyze_obj.run_job(input_file)
            result.info["step"] = "2"  # because MEML jobs are stage 2
            second_stage_results.append(result)

        except AnalysisError as ae:
            log.warning(f"Analysis Error encountered: {str(ae)}")

    # from the random genos, get anything that has h2 >= 0.1

    total_results = results + second_stage_results
    # call post processing
    args = request_id, analysis_request, total_results
    app.send_task("post_process", args=args)


def _run_analyze(request_id, analysis_request, input_files, results):
    # pop 1 from input_files
    input_file, input_files = input_files[0], input_files[1:]
    analyze_obj = pipeline_analyze.get_analyze_object(analysis_request)

    try:
        # run analysis on input file, TODO: call Analyze.run_job() here
        result = analyze_obj.run_job(input_file)
        results.append(result)
    except AnalysisError as ae:
        log.error("Encountered error %s", str(ae))

    finally:
        if not input_files:
            if not results:
                raise AnalysisError("No job results generated for post processing.")
            args = request_id, analysis_request, results
            if analyze_obj.exptloc_analysis_pattern == MEML:
                app.send_task("run_second_stage_analysis", args=args, queue="ASREML")
            else:
                app.send_task("post_process", args=args)
        else:
            args = request_id, analysis_request, input_files, results
            app.send_task("run_asreml_analyze", args=args, queue="ASREML")
        gc.collect()  # mentioned in StackOverflow becuse Rpy2 is using high RAM
