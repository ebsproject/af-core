--liquibase formatted sql

--changeset postgres:update_oft_rcbd_property_meta context:template splitStatements:false rollbackSplitStatements:false
--comment: BA2-190 BA-DB: Update property_meta of OFT_RCBD method



UPDATE af.property_meta
SET value='randRCBDirri_OFT'
WHERE value = 'randRCBD_OFT';

UPDATE af.property_meta
SET value='Rscript randRCBDirri_OFT.R --entryList "RCBD_OFT_SD_0001.lst" --nTrial 3 -o "Output1" -p "D:/Results"'
WHERE id = 309 and code = 'syntax';
