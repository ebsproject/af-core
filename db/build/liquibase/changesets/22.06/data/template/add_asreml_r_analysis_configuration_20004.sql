--liquibase formatted sql

--changeset postgres:add_20004_ac_data context:template splitStatements:false rollbackSplitStatements:false
--comment: Add analysis configuration data for ASREML-R engine 20004


-- config_20004.cfg
WITH analysis_config AS (
	INSERT INTO af.property(
		code, "name", "label", description, "type", data_type
	) VALUES (
		'config_20004.cfg', 'Alpha-Lattice Multi location - Asreml-R',
		'Alpha-Lattice Multi location - Asreml-R', 
		'Alpha-Lattice multi loc, single year and univariate trial with options for spatial adjustment', 
		'catalog_item', 'character varying'
	) RETURNING id
)
INSERT INTO af.property_config(
	order_number, creation_timestamp, creator_id,
	is_void, property_id, config_property_id, is_layout_variable
) VALUES (
	1, 'now()', '1', false, 
	(SELECT id FROM af.property WHERE code = 'analysis_config'), 
	(SELECT id FROM analysis_config), false
);

-- add loc as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    1, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'loc' AND data_type = 'factor'), false
);


-- add expt as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    2, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'expt' AND data_type = 'factor'), false
);


-- add entry as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    3, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'entry' AND data_type = 'factor'), false
);


-- add plot as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    4, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'plot' AND data_type = 'factor'), false
);


-- add col as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    5, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'col' AND data_type = 'factor'), false
);


-- add row as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    6, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'row' AND data_type = 'factor'), false
);


-- add rep as stat factor
INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    7, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'config_20004.cfg'),
    (SELECT id FROM af.property WHERE code = 'rep' AND data_type = 'factor'), false
);


-- add 20004 metadata

WITH analysis_config AS (
    SELECT id FROM af.property WHERE code = 'config_20004.cfg'
)
INSERT INTO af.property_meta(code,value,property_id) VALUES
    ('config_version', '1', (SELECT id FROM analysis_config)),
    ('date', '20-March-2022', (SELECT id FROM analysis_config)),
    ('author', 'Pedro Barbosa', (SELECT id FROM analysis_config)),
    ('email', 'p.medeiros@cgiar.org', (SELECT id FROM analysis_config)),
    ('engine', 'ASREML-R', (SELECT id FROM analysis_config)),
        ('design',  'Alpha-Lattice', (SELECT id FROM analysis_config)),
    ('trait_level', 'plot', (SELECT id FROM analysis_config)),
        ('analysis_objective', 'prediction', (SELECT id FROM analysis_config)),
        ('exp_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('loc_analysis_pattern', 'multi', (SELECT id FROM analysis_config)),
        ('year_analysis_pattern', 'single', (SELECT id FROM analysis_config)),
        ('trait_pattern', 'univariate', (SELECT id FROM analysis_config));

-- add asreml options to 20004
SELECT af.add_analysis_config_property(
    'asreml_opt1', 'asreml_opt1', 'asreml_opt1',
    '', 'asreml_options', 'config_20004.cfg');


-- add formula to 20004
SELECT af.add_analysis_config_property(
    'formula_opt1', 'Univariate. Replicate as fixed and genotype as random effect', 'Univariate. Replicate as fixed and genotype as random effect',
    'fixed = {trait_name} ~ loc + rep:loc, random = ~ entry + entry:loc + rep:block', 'formula', 'config_20004.cfg');


-- add residual to 20004
SELECT af.add_analysis_config_property(
    'residual_opt1', 'Univariate homogeneous variance model', 'Univariate homogeneous variance model',
    '~dsum(~id(units)|loc)', 'residual', 'config_20004.cfg');


-- add residual to 20004
SELECT af.add_analysis_config_property(
    'residual_opt2', 'Autoregressive order 1 spatial structure (AR1row x AR1col)', 'Autoregressive order 1 spatial structure (AR1row x AR1col)',
    '~dsum(~ar1(row):ar1(col)|loc)', 'residual', 'config_20004.cfg');


-- add residual to 20004
SELECT af.add_analysis_config_property(
    'residual_opt3', 'Autoregressive order 1 spatial structure for rows (AR1row x IDcol)', 'Autoregressive order 1 spatial structure for rows (AR1row x IDcol)',
    '~dsum(~ar1(row):id(col)|loc)', 'residual', 'config_20004.cfg');


-- add residual to 20004
SELECT af.add_analysis_config_property(
    'residual_opt4', 'Autoregressive order 1 spatial structure for cols (IDrow x AR1col)', 'Autoregressive order 1 spatial structure for cols (IDrow x AR1col)',
    '~dsum(~id(row):ar1(col)|loc)', 'residual', 'config_20004.cfg');


-- add prediction to 20004
SELECT af.add_analysis_config_property(
    'g', 'G', 'G',
    'entry', 'prediction', 'config_20004.cfg');


-- add prediction to 20004
SELECT af.add_analysis_config_property(
    'gxe', 'GxE', 'GxE',
    'loc:entry', 'prediction', 'config_20004.cfg');


