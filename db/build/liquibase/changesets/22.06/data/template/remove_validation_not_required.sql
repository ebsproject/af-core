--liquibase formatted sql

--changeset postgres:remove_validation_not_required context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1301 Remove the validation of the parameters that are not required in BADB



update af.property_config set is_required = false where property_id = 231 and config_property_id in (72, 73); --RCBD|OFT
update af.property_config set is_required = false where property_id = 228 and config_property_id in (72, 73); --IBD OFT


--update the validation rule for entry list
update af.property_rule set "expression" = 'totalEntries<15' 
where property_config_id in 
(select id from af.property_config where config_property_id in (select id from af.property where code='entryList') 
and property_id = 228) and order_number = 3;