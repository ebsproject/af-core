--liquibase formatted sql

--changeset postgres:void_property context:template splitStatements:false rollbackSplitStatements:false
--comment: BA-261 Void property meta

UPDATE af.property SET is_void = TRUE WHERE code IN (
    'config_00001.cfg',
    'config_00002.cfg',
    'config_00003.cfg',
    'config_00004.cfg',
    'config_00005.cfg',
    'config_00006.cfg',
    'config_00007.cfg',
    'config_00008.cfg'
) AND is_void = FALSE;

--rollback UPDATE af.property SET is_void = FALSE WHERE code IN (
--rollback     'config_00001.cfg',
--rollback     'config_00002.cfg',
--rollback     'config_00003.cfg',
--rollback     'config_00004.cfg',
--rollback     'config_00005.cfg',
--rollback     'config_00006.cfg',
--rollback     'config_00007.cfg',
--rollback     'config_00008.cfg'
--rollback ) AND is_void = TRUE;
