--liquibase formatted sql

--changeset postgres:modify_pred_statement_config_20002 context:template splitStatements:false rollbackSplitStatements:false
--comment: Add block factor to cfg 20003 and 20004

-- Fix for incorrect statement
UPDATE af.property_config
SET config_property_id = (SELECT id from af.property WHERE code='g' AND statement='entry')
WHERE property_id = (SELECT id from af.property WHERE code='config_20002.cfg')
AND config_property_id = (SELECT id from af.property WHERE code='g' AND statement='ge');