--liquibase formatted sql

--changeset postgres:add_block_factors to analysis_module_fields context:template splitStatements:false rollbackSplitStatements:false
--comment: Add block factor to cfg 20003 and 20004

-- add new "factors": block, colblock and rowblock to analysis_module_fields config

INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    8, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'analysis_module_fields'),
    (SELECT id FROM af.property WHERE code = 'block' AND data_type = 'factor'), false
);

INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    8, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'analysis_module_fields'),
    (SELECT id FROM af.property WHERE code = 'rowblock' AND data_type = 'factor'), false
);


INSERT INTO af.property_config (
    order_number, creation_timestamp, creator_id, is_void,property_id,
    config_property_id, is_layout_variable
) VALUES (
    9, 'now()', '1', false,
    (SELECT id FROM af.property WHERE code = 'analysis_module_fields'),
    (SELECT id FROM af.property WHERE code = 'colblock' AND data_type = 'factor'), false
);