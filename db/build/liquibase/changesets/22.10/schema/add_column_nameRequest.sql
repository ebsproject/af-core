--liquibase formatted sql

--changeset postgres:add_column_nameRequest context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1516 Adding attribute nameRequest to table to save the name of request on ARM


ALTER TABLE IF EXISTS af.request
    ADD COLUMN name character varying(200);
ALTER TABLE IF EXISTS af.request
    ADD CONSTRAINT name_unique UNIQUE (name);

--Revert Changes
--rollback ALTER TABLE af.request DROP CONSTRAINT "name_unique";
--rollback ALTER TABLE af.request DROP COLUMN name;

