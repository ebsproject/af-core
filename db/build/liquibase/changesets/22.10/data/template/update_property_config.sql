--liquibase formatted sql

--changeset postgres:update_property_config.sql context:template splitStatements:false rollbackSplitStatements:false
--comment: Update property config



UPDATE af.property SET statement='~dsum(~id(units) | loc)' WHERE statement='~ ~dsum(~id(units) | loc)';