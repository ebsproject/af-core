openapi: 3.0.0
info:
  description: |
    EBS Analytics Frameworks API reference.
    
    [![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/4446227-eeb6ceec-cec9-4c8c-a339-a3fe741b67c7?action=collection%2Ffork&collection-url=entityId%3D4446227-eeb6ceec-cec9-4c8c-a339-a3fe741b67c7%26entityType%3Dcollection%26workspaceId%3D9d6dd7d7-06fb-4724-ae2a-8c6ec649b0b3#?env%5BEBS%5D=W3sia2V5IjoiQWZBcGlVcmwiLCJ2YWx1ZSI6IiIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoiQWNjZXNzVG9rZW4iLCJ2YWx1ZSI6IiIsImVuYWJsZWQiOnRydWV9XQ==)
    
    
  version: "v1"
  title: EBS Analytics
servers:
  # Added by API Auto Mocking Plugin
  # Added by API Auto Mocking Plugin
  - description: SwaggerHub API Auto Mocking
    url: https://virtserver.swaggerhub.com/ebs_analytics/ebs-analytics/v1
  - description: Local instance
    url: https://baapi.local/
tags:
  - name: Requests
    description: Analysis requests made to the af pipeline
  - name: Properties
    description: | 
      Analysis property is a general resource for values of different types required by AF pipeline to submit an analaysis request.
  - name: Analysis Config
    description: |
      Model configuration to run the analysis. It is a type of property resource.
paths:
  /v1/requests:
    get:
      tags: 
        - Requests
      summary: Lists submitted analysis requests in descending order by submitted date.
      operationId: listRequests
      parameters:
        - name: requestorId
          in: query
          description: Id of the requestor.
          schema:
            type: string
        - name: crop
          in: query
          description: Id of the requestor.
          schema:
            type: string
        - name: organization
          in: query
          description: Id of the requestor.
          schema:
            type: string
        - name: status
          in: query
          description: Status of the analysis request.
          schema:
            type: string
            enum:
              - PENDING
              - IN-PROGRESS
              - DONE
              - FAILURE
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/pageSize'
      responses:
        '200':
          description: List of analysis requests
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/AnalysisRequestListResponse'
    post:
      tags:
        - Requests
      summary: Submit new analysis requests.
      operationId: submitRequest
      responses:
        '201':
          description: Submitted
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/AnalysisRequest'
        '400':
          description: Bad Request.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
      requestBody:
        $ref: '#/components/requestBodies/AnalysisRequestParameters'
  /v1/requests/{requestId}:
    get:
      tags:
        - Requests
      summary: Get the Analysis request with given Id.
      description: |
        Gets the list of formulas in analysis config. Formula is a property resource.
      operationId: getRequestById
      parameters:
        - name: requestId
          in: path
          description: Request Id.
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/AnalysisRequestResponse'
        '400':
          description: Bad Request.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '500':
          description: Server Error.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /v1/properties:
    get:
      tags:
        - Properties
      summary: List analysis properties.
      description: |
        Lists properties belonging to given property root. Property root value is an enum with following options,
          
          objective --> Analysis Objective
          
          trait_pattern --> Trait Pattern
          
          exptloc_analysis_pattern --> Experiment Location Analysis Pattern
          
      operationId: listProperties
      parameters:
        - name: propertyRoot
          in: query
          description: Parent property for which child properties needs to be fetched.
          required: true
          schema:
            type: string
            enum:
              - objective
              - trait_pattern
              - exptloc_analysis_pattern
        - name: isActive
          in: query
          description: Whether the property is active in the system.
          schema:
            type: boolean
            default: true
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/pageSize'
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/PropertyListResponse'
        '400':
          description: Bad request.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '500':
          description: Server Error.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /v1/analysis-configs:
    get:
      tags:
        - Analysis Config
      summary: List analysis configurations.
      description: |
        Gets the list of analysis model configurations for given query paramters. Analysis config is a property resource.
      operationId: listAnalysisConfigs
      parameters:
        - name: engine
          in: query
          description: Filter configs by analysis engine.
          schema:
            type: string
        - name: design
          in: query
          description: Filter by design type.
          schema:
            type: string
        - name: traitLevel
          in: query
          description: Filter by trait level
          schema:
            type: string
        - name: analysisObjective
          in: query
          description: Filter by analysis objective.
          schema:
            type: string
        - name: experimentAnalysisPattern
          in: query
          description: Filter by experiment analysis pattern
          schema:
            type: string
        - name: locationAnalysisPattern
          in: query
          description: Filter by location analysis pattern.
          schema:
            type: string
        - name: traitPattern
          in: query
          description: Filter by trait pattern.
          schema:
            type: string
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/pageSize'
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/PropertyListResponse'
        '400':
          description: Bad Request.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '500':
          description: Server Error.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /v1/analysis-configs/{analysisConfigId}/formulas:
    get:
      tags:
        - Analysis Config
      summary: List of formulas in given analysis configuration.
      description: |
        Gets the list of formulas in analysis config. Formula is a property resource.
      operationId: listAnalysisConfigFormulas
      parameters:
        - name: analysisConfigId
          in: path
          description: Property Id of the analysis config.
          schema:
            type: string
          required: true
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/pageSize'
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/PropertyListResponse'
        '400':
          description: Bad Request.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '500':
          description: Server Error.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
  /v1/analysis-configs/{analysisConfigId}/residuals:
    get:
      tags:
        - Analysis Config
      summary: List of residuals in given analysis configuration.
      description: |
        Gets the list of residuals in analysis config. Formula is a property resource.
      operationId: listAnalysisConfigResiduals
      parameters:
        - name: analysisConfigId
          in: path
          description: Property Id of the analysis config.
          schema:
            type: string
          required: true
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/pageSize'
      responses:
        '200':
          description: Ok
          content:
            application/json:
              schema:
                  $ref: '#/components/schemas/PropertyListResponse'
        '400':
          description: Bad Request.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'
        '500':
          description: Server Error.
          content:
            applications/json:
              schema:
                $ref: '#/components/schemas/ErrorResponse'

components:
  parameters:
    page:
      description: 'Used to request a specific page of data to be returned.
        The page indexing starts at 0 (the first page is ''page''= 0). Default is `0`.'
      example: '0'
      in: query
      name: page
      required: false
      schema:
        type: integer
    pageSize:
      description: The size of the pages to be returned. Default is `1000`.
      example: 1000
      in: query
      name: pageSize
      required: false
      schema:
        type: integer
  requestBodies:
    AnalysisRequestParameters:
      content:
        application/json:
          schema:
              $ref: '#/components/schemas/AnalysisRequestParameters'
  schemas:
    Metadata:
      type: object
      properties:
        pagination:
          $ref: '#/components/schemas/Pagination'
    Pagination:
      properties:
        pageSize:
          default: 1000
          description: The number of data elements returned, aka the size of the current page. If the requested page does not have enough elements to fill a page at the requested page size, this field should indicate the actual number of elements returned.
          example: 1000
          type: integer
        currentPage:
          default: 0
          description: The index number for the returned page of data. This should always match the requested page number or the default page (0).
          example: 0
          type: integer
        totalCount:
          description: The total number of elements that are available on the server and match the requested query parameters.
          example: 1000
          type: integer
        totalPages:
          description: "The total number of pages of elements available on the server. This should be calculated with the following formula. \n\ntotalPages = CEILING( totalCount / requested_page_size)"
          example: 1
          type: integer
      required:
      - pageSize
      - currentPage
      type: object
    
    ErrorResponse:
      type: object
      properties:
        errorMsg:
          type: string
          description: Reason or cause of errors.
    AnalysisRequest:
      type: object
      properties:
        requestId:
          type: string
        name:
          type: string
        requestorId:
          type: string
        crop:
          type: string
          description: Name of the crop
        institute:
          type: string
          description: Name of the institute for which the analysis is submitted.
        analysisType:
          type: string
          enum:
            - ANALYZE
            - RANDOMIZE
        status:
          type: string
          enum:
            - PENDING
            - IN-PROGRESS
            - DONE
            - FAILURE
        jobs:
          type: array
          description: List of the analysis jobs associated with the request.
          items:
            $ref: '#/components/schemas/Job'
        createdOn:
          type: string
          format: date-time
        modifiedOn:
          type: string
          format: date-time
        resultDownloadRelativeUrl:
          type: string
          description: Relative url for downloading analysis result.
        experiments:
          type: array
          items:
            $ref: '#/components/schemas/Experiment'
        traits:
          type: array
          items:
            $ref: '#/components/schemas/Trait'
        analysisObjectiveProperty:
          $ref: '#/components/schemas/Property'
        analysisConfigProperty:
          $ref: '#/components/schemas/Property'
        expLocAnalysisPatternProperty:
          $ref: '#/components/schemas/Property'
        configFormulaProperty:
          $ref: '#/components/schemas/Property'
        configResidualProperty:
          $ref: '#/components/schemas/Property'
        traitAnalysisPatternProperty:
          $ref: '#/components/schemas/Property'
    AnalysisRequestParameters:
      type: object
      properties:
        name:
          type: string
          description: Name (readable) to be used for the request
        dataSource:
          type: string
          enum:
            - EBS
            - BRAPI
        dataSourceUrl:
          type: string
          description: Base API url of datasource instance.
        dataSourceAccessToken:
          type: string
          description: Bearer token to access datasource.
        crop:
          type: string
          description: Name of the crop
        requestorId:
          type: string
          description: Id of the user who submits analysis request.
        institute:
          type: string
          description: Name of the institute for which the analysis is submitted.
        analysisType:
          type: string
          enum:
            - ANALYZE
            - RANDOMIZE
        experiments:
          type: array
          items:
            $ref: '#/components/schemas/Experiment'
        traits:
          type: array
          items:
            $ref: '#/components/schemas/Trait'
        analysisObjectivePropertyId:
          type: string
          description: Property Id of selected analysis objective.
        analysisConfigPropertyId:
          type: string
          description: Property Id of selected analysis configuration.
        expLocAnalysisPatternPropertyId:
          type: string
          description: Property Id of selected experiment location analysis pattern.
        configFormulaPropertyId:
          type: string
          description: Property Id of the formula to run the analysis.
        configResidualPropertyId:
          type: string
          description: Property Id of the residual for the analysis model.
        traitAnalysisPatternPropertyId: 
          type: string
          description: Property Id of the trait analysis pattern 
      required:
        - dataSource
        - dataSourceUrl
        - dataSourceAccessToken
        - experiments
        - occurrences
        - traits
        - analysisObjectivePropertyId
        - analysisConfigPropertyId
        - expLocAnalysisPatternPropertyId
        - configFormulaPropertyId
        - configResidualPropertyId
        - traitAnalysisPatternPropertyId
    AnalysisRequestListResponse:
      type: object
      properties:
        metadata:
          $ref: '#/components/schemas/Metadata'
        result:
          type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/AnalysisRequest'
    AnalysisRequestResponse:
      type: object
      properties:
        result:
          $ref: '#/components/schemas/AnalysisRequest'
    PropertyListResponse:
      type: object
      properties:
        metadata:
          $ref: '#/components/schemas/Metadata'
        result:
          type: object
          properties:
            data:
              type: array
              items:
                $ref: '#/components/schemas/Property'
    Experiment:
      type: object
      properties:
        experimentId:
          type: string
          description: Id of the experiment
        experimentName:
          type: string
          description: Name of the experiment.
        occurrences:
          type: array
          description: "*Required for EBS datasource."
          items:
            $ref: '#/components/schemas/Occurrence'
      required:
        - experimentId
    Trait:
      type: object
      properties:
        traitId:
          type: string
          description: Id of the Trait.
        traitName:
          type: string
          description: Name of the Trait.
      required:
        - traitId
    Occurrence:
      type: object
      properties:
        occurrenceId:
          type: string
          description: Id of the Occurrence.
        occurrenceName:
          type: string
          description: Name of the Occurrence.
        locationId:
          type: string
          description: Id of the Location.
        locationName:
          type: string
          description: Name of the Location.
      required:
        - occurrenceId
    Job:
      type: object
      properties:
        jobId: 
          type: string
          description: Id of the job.
        jobName: 
          type: string
          description: Name of the job.
        status:
          type: string
          enum:
            - PENDING
            - IN-PROGRESS
            - DONE
            - FAILED
            - ERROR
        statusMessage: 
          type: string
          description: Status message of the job.
        traitName:
          type: string
          description: Trait name of the job
        locationName:
          type: string
          description: Name of the location.
        startTime:
          type: string
          format: date-time
        endTime:
          type: string
          format: date-time
    Property:
      type: object
      properties:
        propertyId:
          type: string
          description: Id of the property
        propertyName:
          type: string
          description: Name of the property
        propertyCode:
          type: string
          description: Property code.
        label:
          type: string
          description: Label for user view.
        type:
          type: string
          description: Classifier of properties within its context (e.g. catalog_item, catalog_root)
        createdOn:
          type: string
          format: date-time
        modifiedOn:
          type: string
          format: date-time
        createdBy:
          type: string
          description: Id of the user who created the property.
        modifiedBy:
          type: string
          description: Id of the user who modified the property.
        isActive:
          type: boolean
          default: true
          description: Whether the property is active in the system.
        statement:
          type: string
          description: A command, instruction, piece of code, etc., associated to the property